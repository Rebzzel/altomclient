<?php
namespace app\modules;

use httpclient;
use facade\Json;
use std, gui, framework, app;


class API extends AbstractModule
{
    private $data = [];
    private $url = "http://api.altom.ml";
    private $cookies = [];
    private $hash;
    
    
    // отправка запроса на сервер altom
    static function query($method, $array)
    {
        $respounce = app()->module('API')->httpClient->get(app()->module('API')->url,[
        'method' => $method,
        'json' => urldecode(Json::encode($array))
        ]);
        echo '[SERVER CODE] ' . $respounce->statusCode() . " \n";
        $c = $respounce->cookies();
        app()->module('API')->cookies = $c;
        return $respounce->body();
    }
    
    //функция автаризации
    static function login($mail, $password)
    {
        $res = API::query('login', [
            'mail' => $mail,
            'password' => $password
        ]);
        if ($res['success'] == 'yes')
        {
            app()->module('API')->data = [
                'id' => $res['id'],
                'hash' => $res['hash']
            ];
            global $hash;
            global $id;
            $hash = app()->module('API')->data['hash'];
            $id = app()->module('API')->data['id'];
            return $res;
        } 
            else 
        {
            return $res;
        }
    }
    
    //функция регистрации 
    static function register($name, $lastname, $mail, $password)
    {
        $res = API::query('reg', [
            'name' => $name,
            'lastname' => $lastname,
            'mail' => $mail,
            'password' => $password
        ]);

        if ($res['success'] == 'yes')
        {
            return $res;
        } 
            else 
        {
            return $res['error'];
        }
    }
    
}