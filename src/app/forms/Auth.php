<?php
namespace app\forms;

use php\xml\XmlProcessor;
use action\Animation;
use app\modules\API;
use facade\Json;
use libs\UXBlur;
use std, gui, framework, app;
use php\gui\event\UXEvent; 


class Auth extends AbstractForm
{
    function error($text){
        $this->labelAlt->text = $text;
        $this->chatterAnim->enable();
        waitAsync(240, function () use ($event){
            $this->chatterAnim->disable();
        });
        Animation::moveTo($this->label, 250, 76, 64);
        Animation::fadeIn($this->labelAlt, 250);
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        Animation::fadeOut($this, 1, function () use ($event){
            new UXBlur("./dll/DNWBU.dll")->SetBlur($this->title);
            Animation::fadeIn($this, 300);
        });
    }

    /**
     * @event button.action 
     */
    function doButtonAction(UXEvent $e = null)
    {
        if ($this->edit->text)
        {
            if ($this->passwordField->text)
            {
                $res = API::login($this->edit->text, $this->passwordField->text);
                var_dump($res);
                if (!$res)
                {
                    $this->error("Сервер недоступен.");
                }
                
                if ($res['error'] == "invalid login or password")
                {
                    $this->error("Неправильный логин или пароль.");
                }
                
                if ($res['success'] == 'yes')
                {
                    Animation::fadeOut($this, 300, function () use ($event){
                        $this->loadForm("Chat");
                    }); 
                }
            }
        }
    }

    /**
     * @event buttonAlt.action 
     */
    function doButtonAltAction(UXEvent $e = null)
    {
        Animation::fadeOut($this, 250, function () use ($event){
            $this->loadForm("reg");
        }); 
    }

    /**
     * @event button3.action 
     */
    function doButton3Action(UXEvent $e = null)
    {    
        Animation::fadeOut($this, 250, function () use ($event) {
            $this->hide();
        });
    }

}
