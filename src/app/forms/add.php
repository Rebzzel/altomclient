<?php
namespace app\forms;

use app\modules\API;
use libs\UXBlur;
use action\Animation;
use php\gui\framework\AbstractForm;
use php\gui\event\UXEvent; 
use php\gui\event\UXWindowEvent; 


class add extends AbstractForm
{

    /**
     * @event button3.action 
     */
    function doButton3Action(UXEvent $e = null)
    {
        Animation::fadeOut($this, 250, function () use ($event) {
            $this->hide();
        });
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        Animation::fadeOut($this, 1, function () use ($event){
            new UXBlur("./dll/DNWBU.dll")->SetBlur($this->title);
            Animation::fadeIn($this, 300);
        });
    }

    /**
     * @event button.action 
     */
    function doButtonAction(UXEvent $e = null)
    {    
        global $hash;
        $row = API::query("addroom", [ 'hash' => $hash, 'id' => $this->edit->text ]);
        if ($row['success'] == "yes")
        {
            Animation::fadeOut($this, 250, function () use ($event) {
                app()->form("Chat")->rooms();
                $this->hide();
            });
        } elseif ($row['error'] == "room not isset") {
            pre("Комноты не существует.");
        } elseif ($row['error'] == "room isset in this user"){
            pre("Эта комната у вас уже существует.");
        }
    }

}
