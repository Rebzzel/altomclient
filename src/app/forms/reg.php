<?php
namespace app\forms;

use app\modules\API;
use facade\Json;
use std, gui, framework, app;
use php\gui\event\UXWindowEvent; 
use php\gui\event\UXEvent; 


class reg extends AbstractForm
{
    function error($text){
        $this->labelAlt->text = $text;
        Animation::moveTo($this->label, 240, 76, 64);
        Animation::fadeIn($this->labelAlt, 250);
    } 
    
    /**
     * @event button.action 
     */
    function doButtonAction(UXEvent $e = null)
    {
        if ($this->edit->text)
        {
            if ($this->passwordField->text)
            {
                
                $res = API::register($this->editAlt->text, $this->edit3->text, $this->edit->text, $this->passwordField->text);
                
                if ($res['error'] == "user isset")
                {
                    $this->error("Такой E-mail уже зарегистрирован.");
                }
                
                if ($res['error'] == "server error")
                {
                    $this->error("Ошибка сервера.");
                }
                
                if ($res['success'] == 'yes')
                {
                    Animation::fadeOut($this, 300, function () use ($event){
                        $this->loadForm("Auth");
                    }); 
                }
            }
        }
    }

    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        Animation::fadeOut($this->panel, 1, function () use ($event){
            new UXBlur("./dll/DNWBU.dll")->SetBlur($this->title);
            Animation::fadeIn($this->panel, 300);
        });
    }

    /**
     * @event button3.action 
     */
    function doButton3Action(UXEvent $e = null)
    {
        Animation::fadeOut($this, 250, function () use ($event) {
            $this->hide();
        });
    }



}
