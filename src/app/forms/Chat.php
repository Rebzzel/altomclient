<?php
namespace app\forms;

use php\gui\layout\UXHBox;
use php\gui\UXImageView;
use php\gui\UXLabel;
use php\gui\layout\UXPanel;
use php\gui\UXImage;
use app\modules\API;
use std, gui, framework, app;
use php\gui\event\UXEvent; 
use php\gui\event\UXWindowEvent; 


class Chat extends AbstractForm
{

    function rooms(){
        global $hash;
        $rooms = API::query("getmyrooms", [ 'hash' => $hash ]);
        foreach ($rooms['data'] as $room)
        {
            $row = API::query("getroom", [ 'id' => $room ]);
            $data = $row['data'];
            $l = new UXLabel($data['name']);
            $l->textColor = "#fff";
            $l->height = 50;
            $img = new UXImageView();
            if($data['avatar'] == "def" || !$data['avatar']) {
                $img->image = new UXImage("res://.data/img/ic_account_circle_white_18dp.png");
            } else {
                $img->image = new UXImage($data['avatar']);
            }
            $img->size = [ 30, 30 ];
            $hbox = new UXHBox();
            $hbox->spacing = 8;
            $hbox->paddingLeft = 8;
            $hbox->alignment = "CENTER_LEFT";
            $hbox->add($img);
            $hbox->add($l);
            $this->listView->items->add($hbox);
        }
    }
    
    /**
     * @event showing 
     */
    function doShowing(UXWindowEvent $e = null)
    {    
        global $hash;
        global $id;
        $res   = API::query('userget', [ 'id' => $id ]);
        $this->label->text = $res['data']['name'] . " " . $res['data']['lastname'];
        if (!$res['data']['avatar'] == 'def' && $res['data']['avatar'] != null){
            $this->image->image = new UXImage($res['avatar']);
        }
        $this->listView->padding = 0;
        $this->rooms();
    }


    /**
     * @event show 
     */
    function doShow(UXWindowEvent $e = null)
    {    
        Animation::fadeOut($this, 1, function () use ($event){
            new UXBlur("./dll/DNWBU.dll")->SetBlur($this->title);
            Animation::fadeIn($this, 300);
        });
    }

    /**
     * @event button3.action 
     */
    function doButton3Action(UXEvent $e = null)
    {
        Animation::fadeOut($this, 250, function () use ($event) {
            $this->hide();
        });
    }

    /**
     * @event button.action 
     */
    function doButtonAction(UXEvent $e = null)
    {    
        app()->showForm("add");
    }

}
